package Components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchScreen {
    WebDriver driver;

    public SearchScreen (WebDriver driver){
        this.driver=driver;
    }

    public WebElement getSearchInput() {
      return driver.findElement(By.xpath("//input[@class='gLFyf gsfi']"));
    }

}
