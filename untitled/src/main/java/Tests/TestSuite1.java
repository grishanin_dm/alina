package Tests;

import Components.ResultScreen;
import Components.SearchScreen;
import com.google.common.annotations.VisibleForTesting;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.List;

public class TestSuite1 extends Base {

    @Test(priority = 1)
    public void VerifyResult() {
       SearchScreen searchScreen = new SearchScreen(driver);
       searchScreen.getSearchInput().sendKeys("погода" + Keys.ENTER);

        ResultScreen resultScreen = new ResultScreen(driver);
        String result = resultScreen.getTopResult().getText();

        Assert.assertTrue(result.contains("Днипро"), "Result should has Киев");
    }

    @Test(priority = 2)
    public void VerifyCountOfResults() {
        List<WebElement> results = driver.findElements(By.cssSelector("div[class='g']"));

        Assert.assertTrue(results.size() > 3, "List of results should be more than 3");
    }

    @AfterSuite
    public void TearDown(){
        driver.quit();
    }
}
