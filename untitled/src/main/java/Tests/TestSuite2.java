package Tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.List;

public class TestSuite2 extends Base {


    @Test(priority = 1)
    public void VerifyResult() {
        // driver.findElement(By.name("q")).sendKeys("погода" + Keys.ENTER);   By name
        // driver.findElement(By.cssSelector("input[class='gLFyf gsfi']")).sendKeys("погода" + Keys.ENTER);   // By css
        driver.findElement(By.xpath("//input[@class='gLFyf gsfi']")).sendKeys("погода" + Keys.ENTER);   // By xpath
        String result = driver.findElement(By.id("wob_loc")).getText();

        Assert.assertTrue(result.contains("Днипро"), "Result should has Киев");
    }

    @Test(priority = 2)
    public void VerifyCountOfResults() {
        List<WebElement> results = driver.findElements(By.cssSelector("div[class='g']"));

        Assert.assertTrue(results.size() > 3, "List of results should be more than 3");
    }

    @AfterSuite
    public void TearDown(){
        driver.quit();
    }
}
